<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Product::truncate();

        $faker = \Faker\Factory::create();

        \App\Product::create([
            'user_id'     => 1,
            'title'    => '$faker->name',
            'price'    => '$faker->text',
            'description'    => '$faker->text',
        ]);

        for ($i = 0; $i < 10; ++$i) {
            \App\Product::create([
                'user_id'    => '1',
                'title'    => $faker->title,
                'price'    => $faker->numberBetween($min = 1500, $max = 6000),
                'description'    => $faker->paragraph,
            ]);
        }
    }
}
