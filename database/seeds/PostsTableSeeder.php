<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Post::truncate();

        $faker = \Faker\Factory::create();

        \App\Post::create([
            'author_id'     => 1,
            'title'    => '$faker->name',
            'content'    => '$faker->text',
        ]);

        for ($i = 0; $i < 10; ++$i) {
            \App\Post::create([
                'author_id'    => '1',
                'title'    => $faker->title,
                'content'    => $faker->text,
            ]);
        }
    }
}
