<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Post extends Model
{

    protected $fillable = [
        'author_id', 'title', 'content',
    ];

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
