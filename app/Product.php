<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'author_id', 'title', 'price', 'description'
    ];

    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
