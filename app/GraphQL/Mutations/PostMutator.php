<?php

namespace App\GraphQL\Mutations;
use App\Post;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
class PostMutator
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        // TODO implement the resolver
    }
    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $post = new Post($args);
        $context->user()->posts()->save($post);

        return $post;
    }

    public function delete($rootValue, array $args, GraphQLContext $context)
    {
        $post = Post::findOrFail($args['id']);
        if (!$post) {
            return null;
        }
        $post->delete();
        return $post;

    }

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $post = Post::findOrFail($args['id']);
        if (!$post) {
            return null;
        }
        $post->author_id = $args['author_id'];
        $post->title = $args['title'];
        $post->content = $args['content'];
        $post->save();
        return $post;
    }
}
